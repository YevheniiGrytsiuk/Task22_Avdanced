"use strict";

const button = document.getElementById("box-input-btn");

const tableDiv = document.getElementById('box-tbl-div');

let selectedTd = 0;

button.addEventListener('click', function () {

    const table = document.getElementById("table-in-box");

    if (table != null) {
        tableDiv.removeChild(table);
    }

    const rows = document.getElementById("box-input-row").value;
    const cols = document.getElementById("box-input-col").value;

    let tbl = document.createElement('table');
    tbl.id = "table-in-box";
    tbl.style.width = '100%';

    let tbody = document.createElement('tbody');

    for (let i = 0; i < rows; i++) {
        let tr = document.createElement('tr');

        for (let j = 0; j < cols; j++) {
            let td = tr.insertCell();
            td.appendChild(document.createTextNode(`row: ${i} col: ${j}`));
            
            
            td.id = `${i}/${j}`;
            if(selectedTd && selectedTd.id == td.id )
                td.className = "highlight";
            else
                td.className = "simp";
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }

    tbl.appendChild(tbody);
    tableDiv.appendChild(tbl);
});


tableDiv.addEventListener('click', function (event) {

    let target = event.target;

    if (target.tagName != 'TD') {
        return;
    }

    let targetStyle = window.getComputedStyle(target);

    const eventOffsetX = event.offsetX;
    const targetStyleWidth = parseInt(targetStyle.width);
    const eventOffsetY = event.offsetY;
    const targetStyleHeight = parseInt(targetStyle.height);

    if (eventOffsetX > targetStyleWidth || eventOffsetX < 0 || eventOffsetY > targetStyleHeight || eventOffsetY < 0) {
        target.style.borderColor = "yellow";
        alert(`Border click!!!\nevent offsetX: ${eventOffsetX}\nevent offsetY: ${eventOffsetY}\ntd width: ${targetStyleWidth}\ntd height: ${targetStyleHeight}`);
        target.style.borderColor = "green";
        return;
    }

    if (selectedTd) {
        if (target.className == 'highlight') {
            target.className = 'simp';
            selectedTd = null;
        }
        else if (target.className == "simp") {
            target.className = 'highlight';
            //changing className of actual td in case selectedTd is td from deleted table
            document.getElementById(`${selectedTd.id}`).className = 'simp';
            selectedTd = target;
        }
    }
    else {
        selectedTd = target;
        selectedTd.className = 'highlight';
    }

});